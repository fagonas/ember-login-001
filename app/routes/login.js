import Ember from 'ember';

export default Ember.Route.extend({

    session: Ember.inject.service('session'),
    actions: {
        authenticate(user) {
            user = {
                identification: "admin",
                password: "admin"
            };
            console.log("trying to authenticate with creds: ", user);
            this.get('session').authenticate('authenticator:jwt', user);
        }
    }

});
