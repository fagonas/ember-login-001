import Ember from 'ember';

export default Ember.Component.extend({
    actions: {
        submit() {
            this.sendAction('action', this.get('user'));
            console.log("this credentials: ", this.get('user'));
        },

        authenticate() {
            console.log("authenticate in component js.");
        }

    }
});